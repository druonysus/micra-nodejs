FROM druonysus/micra

ADD node-v8.11.1-linux-x64.tar.xz /opt/

RUN ln -s /opt/node-v8.11.1-linux-x64/bin/node /usr/local/bin/node
