build: download
	docker build -t druonysus/micra-nodejs .

run:
	docker run -it druonysus/micra-nodejs:latest node

push:
	docker push druonysus/micra-nodejs

download:
	wget https://nodejs.org/dist/v8.11.1/node-v8.11.1-linux-x64.tar.xz

